package com.zuitt;
import java.util.HashMap;
import java.util.ArrayList;

public class Activity2 {
    public static void main(String[] args) {
        int[] primeNos = {2,3,5,7,11};
        System.out.println("The first prime number is " + primeNos[0]);
        System.out.println("The second prime number is " + primeNos[1]);
        System.out.println("The third prime number is " + primeNos[2]);
        System.out.println("The fourth prime number is " + primeNos[3]);
        System.out.println("The fifthprime number is " + primeNos[4]);

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoe");
        System.out.println("My friends are: "+friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);
        System.out.println("Our current inventory consists of: " + inventory);
    }
}
