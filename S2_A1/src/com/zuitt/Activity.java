package com.zuitt;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        System.out.println("Input a year to be checked if a leap year");
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        if(year/4==0||(year/100==0&&year/400==0)){
            System.out.println(year+" is a leap year.");
        }else{
            System.out.println(year+" is NOT a  leap year.");
        }


    }
}
