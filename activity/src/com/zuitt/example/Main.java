package com.zuitt.example;

import javax.sound.midi.SysexMessage;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.setContactNumber(new String[]{"+639152468596", "+639228547963"});
        contact1.setAddress(new String[]{"Quezon City", "Makati City"});

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        contact2.setContactNumber(new String[]{"+639162148573", "+639173698541"});
        contact2.setAddress(new String[]{"Caloocan City", "Pasay City"});

        phonebook.getContacts().add(contact1);
        phonebook.getContacts().add(contact2);

        if (phonebook.getContacts().isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : phonebook.getContacts()) {

                System.out.println(contact.getName());

                System.out.println("----------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String number : contact.getContactNumber()) {
                    System.out.println(number);
                }

                System.out.println("----------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println("My home address is in " + contact.getAddress()[0]);
                System.out.println("My office address is in " + contact.getAddress()[1]);

                System.out.println("=============================");
            }
        }
    }

}

