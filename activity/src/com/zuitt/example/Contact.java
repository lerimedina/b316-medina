package com.zuitt.example;

public class Contact {
    private String name;
    private String[] contactNumber;
    private String[] address;
    public Contact(){

    }
    public Contact(String name, String[] contactNumber, String[] address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;

    }
    public String getName(){
        return this.name;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }
    public String[] getAddress(){
        return this.address;
    }
    public void setAddress(String[] addressParams){
        this.address = addressParams;
    }
    public String[] getContactNumber(){
        return this.contactNumber;
    }
    public void setContactNumber(String[] contactNumberParams){
        this.contactNumber = contactNumberParams;
    }

}
