package com.zuitt.activity1;
import java.util.Scanner;
public class Activity1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = sc.nextLine();
        System.out.println("Last Name:");
        String lastName = sc.nextLine();
        System.out.println("First Subject Grade:");
        double firstSubject = new Double(sc.nextLine());
        System.out.println("Second Subject Grade:");
        double secondSubject = new Double(sc.nextLine());
        System.out.println("Third Subject Grade:");
        double thirdSubject = new Double(sc.nextLine());
        System.out.println("Good day, " + firstName + " " + lastName+".");
        double aveGrade = (firstSubject+secondSubject+thirdSubject)/3;
        System.out.println("Your average grade is: " + Math.round(aveGrade));
    }
}
