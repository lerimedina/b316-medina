package com.zuitt.example;

public class Person implements Actions{
    public void sleep(){
        System.out.println("Zzzzzzzzzzzzzz");
    }
    public void run(){
        System.out.println("Running on the road");
    }
    public void code(){
        System.out.println("Coding");
    }
    public void test(){
        System.out.println("Testing");
    }
}
