package com.zuitt.example;

public class StaticPoly {
//    Ability to have multiple methhods of the same name but changes forms based on the number of arguments or the types of arguments
    public int addition(int a, int b){
        return a+b;
    }
//    Overloading by changing the number of arguments
    public int addition(int a, int b, int c){
        return a+b+c;
    }
    public double addition(double a, double b){
        return a+b;
    }
}
