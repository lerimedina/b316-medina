package com.zuitt.example;

import javax.crypto.Cipher;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.run();
        person1.sleep();
        person1.code();
        person1.test();

        StaticPoly staticPoly = new StaticPoly();
        System.out.println(staticPoly.addition(1,5));
        System.out.println(staticPoly.addition(1,5, 10));
        System.out.println(staticPoly.addition(1.43,5.4342));

        Parent parent1 = new Parent("John", 35);
        parent1.speak();
        parent1.greet();
        parent1.greet("John", "Morning");


        Child child = new Child();
        child.speak();
    }
}
