package com.zuitt.example;
import java.util.Scanner;
public class Activity {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed: ");
        Scanner in = new Scanner(System.in);
        int num = 1;
        try{
            num = in.nextInt();
        }catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        int answer = 1;
        int answer2 = 1;
        int counter = 1;
        if(num>1){
            while(counter<=num){
                answer = answer*counter;
                counter++;
            }
            System.out.println("The factorial of " + num+" for while loop is " + answer);
            for(int x=1; x<=num; x++){
                answer2=answer2*x;
            }
            System.out.println("The factorial of " + num +" for for loop is " + answer2);
        }else{
            System.out.println("Cannot input a zero or negative value");
        }
        System.out.println();
        System.out.println();
        System.out.println("/////////////////STRETCH GOALS/////////////////");
        System.out.println();
        System.out.println();
        for(int x=1; x<=5; x++){
            for(int y=0;y<x;y++){
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
