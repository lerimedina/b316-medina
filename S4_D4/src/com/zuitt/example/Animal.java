package com.zuitt.example;

public class Animal {
    private String name;
    private String color;
    public Animal(){

    }
    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }
    public String getColor(){
        return this.color;
    }
    public void setColor(String nameColor){
        this.color = nameColor;
    }
    public void call(){
        System.out.println("Hi, my name is "+ this.name+".");
    }
}
