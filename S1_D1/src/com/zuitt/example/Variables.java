package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
//        Variable declaration
        int age;
        char middle_name;
//        Variable declaration vs. Initialization
        int x;
        int y = 0;
//
        System.out.println("The value of y is "+y);
        y=10;
        System.out.println("The value of y is "+y);

    }
}
