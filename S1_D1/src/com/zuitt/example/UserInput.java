package com.zuitt.example;
import java.util.Scanner;
public class UserInput {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in); //Creating a new Scanner object
        System.out.println("Enter a new username: ");
        String userName = myObj.nextLine();//Reads the user input
        System.out.println("Username is: " + userName);
    }
}
