package com.zuitt.example;
import java.util.Scanner;

public class Exceptions {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); //The scanner object looks for which input to scan. In this case, it is the console (System.in)
//        The scanner object has multiple methods that can be used to get  the expected Value
        /*
        * nextInt() = expects integer
        * nextDouble() = expects double
        * nextLine() = gets the entire line as a string
        */
        System.out.println("Input a number: ");
        int num = 0;
        try{// try to do this statement
            num = sc.nextInt(); // the nextInt() method tells Java that it is expecting an integer value as input.
        }catch (Exception e){//Catch any errors
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        System.out.println("You have entered: " + num);
    }
}
