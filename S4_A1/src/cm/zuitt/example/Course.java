package cm.zuitt.example;

import java.util.Date;

public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private Date startDate;
    private User instructor;

    public Course(){
        this.instructor = new User();
    }
    public Course(String name, String description, int seats, double fee, User instructor){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = new Date();
        this.instructor = instructor;
    }
    public String getName(){
        return this.name;
    }
    public void setName(String nameParams){
        this.name = nameParams;
    }
    public String getDescription(){
        return this.description;
    }
    public void setDescription(String descriptionParams){
        this.description = descriptionParams;
    }
    public int seats(){
        return this.seats;
    }
    public void setSeats(int seatsParams){
        this.seats = seatsParams;
    }
    public double getFee(){
        return this.fee;
    }
    public void setFee(double feeParams){
        this.fee = feeParams;
    }
    public Date getStartDate(){
        return this.startDate;
    }
    public User getInstructor(){
        return this.instructor;
    }
    public void setInstructor(User instructorParams){
        this.instructor = instructorParams;
    }
    public void intro(){
        System.out.println("Welcome to the course " + this.name + ". This course can be described as " + this.description + ". Your instructor for this course is " + this.instructor.getName() + ". Enjoy!");
    }
}
