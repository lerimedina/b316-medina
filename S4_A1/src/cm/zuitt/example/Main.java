package cm.zuitt.example;

public class Main {
    public static void main(String[] args) {
        User user = new User();
        user.setname("Terrence Gaffud");
        user.setAge(25);
        user.setAddress("Quezon City");
        user.setEmail("tgaff@mail.com");
        Course course = new Course();
        course.setName("MACQ004");
        course.setDescription("An Introduction to Java for career-shifters");
        course.setFee(1000.25);
        course.setInstructor(user);
        course.setSeats(3);

        user.introduction();
        course.intro();
    }
}
